"use strict";

// #### Технические требования:
// - В файле `index.html` лежит разметка для двух полей ввода пароля.
// - По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь, иконка меняет свой внешний вид. В комментариях под иконкой - иконка другая, именно она должна отображаться вместо текущей.
// - Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// - Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// - По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// - Если значения совпадают - вывести модальное окно (можно alert) с текстом - `You are welcome`;
// - Если значение не совпадают - вывести под вторым полем текст красного цвета  `Нужно ввести одинаковые значения`
// - После нажатия на кнопку страница не должна перезагружаться
// - Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.

const ftPassword = document.querySelector("#first-password");
const scPassword = document.querySelector("#second-password");
const ftPasswordIcons = Array.from(
  document.querySelectorAll(".icon-first-password")
);
const scPasswordIcons = Array.from(
  document.querySelectorAll(".icon-second-password")
);

const changeIcons = function (arrIcons, password) {
  arrIcons.forEach((el) => {
    el.addEventListener("click", () => {
      arrIcons.forEach((el) => el.classList.remove("hidden"));

      el.classList.add("hidden");
      if (el.classList.contains("fa-eye")) {
        password.type = "password";
      }
      if (el.classList.contains("fa-eye-slash")) {
        password.type = "text";
      }
    });
  });
};

changeIcons(ftPasswordIcons, ftPassword);
changeIcons(scPasswordIcons, scPassword);

const btn = document.querySelector(".btn");
let paragraph = document.createElement("p");

btn.addEventListener("click", (event) => {
  event.preventDefault();
  if (ftPassword.value === "") {
    alert("Enter password");
    return;
  }
  if (ftPassword.value === scPassword.value) {
    alert("You are welcome");
    paragraph.remove();
  } else {
    if (document.contains(paragraph)) {
      paragraph.innerText = "Нужно ввести одинаковые значения";
    } else {
      btn.before(paragraph);
      paragraph.innerText = "Нужно ввести одинаковые значения";
      paragraph.style.color = "red";
    }
  }
});
